import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Fragment, useState, useEffect, useContext} from 'react'
import {Navigate, useNavigate} from "react-router-dom"
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Register () {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("")


	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()


	useEffect(() => {
		if(email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password, confirmPassword])

	function register(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/user/registration`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data !== false){
				Swal.fire({
					title: "Create Successfully",
					icon: "success",
					text: "Proceed to Login"
				})
				localStorage.setItem("token", data.auth)
				setUser(localStorage.getItem("token"))
				navigate("/")
			}else{
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Email is already registered!"
				})
			}
			
		})
		
	}

	return (
	user ?
	<Navigate to ="*"/>
	:
	<Fragment>
		<h1 className="text-center mt-5">Register</h1>
		<Form className="mt-5" onSubmit={event => register(event)}>
		     <Form.Group className="mb-3" controlId="formBasicEmail">
		       <Form.Label>Email address</Form.Label>
		       <Form.Control 
		       		type="email" 
		       		placeholder="Enter email"
		       		value = {email}
		       		onChange = {event => setEmail(event.target.value)}
		       		required />
		       <Form.Text className="text-muted">
		         We'll never share your email with anyone else.
		       </Form.Text>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicPassword">
		       <Form.Label>Password</Form.Label>
		       <Form.Control 
		       		type="password" 
		       		placeholder="Password"
		       		value = {password}
		       		onChange = {event => setPassword(event.target.value)}
		       		required />
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
		       <Form.Label>Verify Password</Form.Label>
		       <Form.Control 
		       		type="password" 
		       		placeholder="Confirm Your Password"
		       		value = {confirmPassword}
		       		onChange = {event => setConfirmPassword(event.target.value)}
		       		required />
		     </Form.Group>
		     {/*In this code block we do conditional rendering depending on the state of our isActive*/}
		     {
		     	isActive ?
		     	<Button variant="primary" type="submit">
		     	  Please enter your registration details
		     	</Button>
		     	:
		     	<Button variant="danger" type="submit" disabled>
		     	  Please enter your registration details
		     	</Button>
		     }
		    
		    <Form.Group className="mt-3">
		       <Form.Text className="mt-5">Already have an account?<a href="/login"> Click here</a> to login</Form.Text>
		     </Form.Group>
		   </Form>

	</Fragment>
	)
}
