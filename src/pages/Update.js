import {useEffect, useState, useContext, Fragment} from 'react'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2'
import {Navigate, useNavigate, Link, useParams} from "react-router-dom"
import '../home.css'

export default function Update(){
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")

	const {productId} = useParams()

	const navigate = useNavigate()

	function updateProduct(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/product/update/${productId}`, {
			method: 'PUT',
			headers:{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(result => result.json())
		.then(data => {
			if(data !== false){
				Swal.fire({
					title: "Successfully",
					icon: "success",
					text: "create product Successfully"
				})
				navigate('/allProduct')
			}else{
				Swal.fire({
					title: "You are not an admin",
					icon: "error",
					text: "Become an admin first"
				})
			}
		})
	}


	return(
		<Fragment>
		<h1 className="text-center mt-5">Update Product</h1>

		<div className="text-center justify-content-center d-flex">		
		<Form className="mt-5" onSubmit={event => updateProduct(event)}>
		     <Form.Group className="mb-3" controlId="formBasicEmail">
		       <Form.Label>Name</Form.Label>
		       <Form.Control
		       		type="text" 
		       		value = {name}
		       		onChange = {event => setName(event.target.value)}
		       		required
		       		size="lg"/>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicPassword">
		       <Form.Label>Description</Form.Label>
		       <Form.Control
		       		type="text" 
		       		value = {description}
		       		onChange = {event => setDescription(event.target.value)}
		       		required
		       		size="lg"/>
		     </Form.Group>

		     <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
		       <Form.Label>Price</Form.Label>
		       <Form.Control 
		       		type="number" 
		       		value = {price}
		       		onChange = {event => setPrice(event.target.value)}
		       		required
		       		size="lg"/>
		     </Form.Group>		    
		     	<Button variant="primary" type="submit">
		     	  Update Product
		     	</Button>
		     		    
		   </Form>

		</div>
		</Fragment>
		)
}