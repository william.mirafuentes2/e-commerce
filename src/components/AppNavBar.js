//import bootstrap classes
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link, NavLink} from 'react-router-dom'
import {useContext, Fragment, useEffect, useState} from 'react'
import UserContext from '../UserContext.js'
import NavDropdown from 'react-bootstrap/NavDropdown';

/*import {Container, Nav, Navbar} from 'react-bootstrap*/

export default function AppNavBar(){

	const {user} = useContext(UserContext)

	return(
		<Navbar bg="dark" variant="dark" expand="lg" className="sticky">
		      <Container>
		        <Navbar.Brand as = {Link} to = "/">Shop</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
		            <Nav.Link as = {NavLink} to = "/activeProducts">Product</Nav.Link>


		            {
		            	user ?
		            	<Fragment>
							<NavDropdown title="Admin DashBoard" id="basic-nav-dropdown">
		            		    <NavDropdown.Item href="/allProduct">All Products</NavDropdown.Item>
		            		    <NavDropdown.Item href="/createProduct">Create Products</NavDropdown.Item>
		            		</NavDropdown>					
		            		<Nav.Link as = {NavLink} to = "/order">Cart</Nav.Link>
		            		<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
	               		</Fragment>
		            	:
		            	<Fragment>
		            		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
		            		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		            	</Fragment>
		            }
  
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)
}