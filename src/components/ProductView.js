import {useState, useEffect, Fragment} from 'react'
import {Container, Row, Col, Card, Button, Table} from 'react-bootstrap'
import {useParams, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function ProductView(){

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [quantity, setQuantity] = useState([])
	const navigate = useNavigate()
	const {productId} = useParams()
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setQuantity(1)
		})
	}, [productId])

	const enroll = (id) => {
		//we are goind to fetch the route of the login
		fetch(`${process.env.REACT_APP_API_URL}/product/${id}`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(result => result.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Successfully enrolled!",
					icon: "success",
					text: "Please wait for the final schedule of the course!"
				})
				navigate('/courses')
			}else{
				Swal.fire({
					title: "Enrollment unsuccessful",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}
	return(
		<Fragment>
		<Table>
		<tbody>
        <tr>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td><Button></Button></td>
        </tr>
        </tbody>
        </Table>
    </Fragment>
		)
}